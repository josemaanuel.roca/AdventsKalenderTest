<!DOCTYPE html>
<html>
<head>
<title>Register Event Form</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>

<link href="./bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="./datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

<link href="css/style.css" rel="stylesheet" type="text/css" media="all">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">

<link href="//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">
<link href="//fonts.googleapis.com/css?family=Raleway+Dots" rel="stylesheet" type="text/css">

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>
	<div class="header-w3l">
		<h1>DealPush Register Form</h1>
	</div>

<div class="main-agileits">
		<div class="sub-main">	
			<form id="contact" action="VerifyCaptcha2.php" method="post" name="eventKalendar" enctype="multipart/form-data">
				<input placeholder="Title" name="title" class="name" type="text" required="">
					
				<textarea placeholder="Description" name="descript" type="text" required></textarea>
					
				<input placeholder="URL" name="url" type="text" required="">
					
				<input placeholder="Image" name="userfile[]" type="file" required="">
                
                <select placeholder="CampaignID" name="campaignID" class="form-control">
                    <option selected="58106a31117784713fdde24e" value="58106a31117784713fdde24e">DealPushBot</option>
					<option value="580df400117784713fdde24d">AdventsKalender</option>
					<option value="586f94a9dd5f84183037346d">DealPushTestHelge</option>
				</select>
					
                <input placeholder="ZipCode" name="zipcode" type="text" required="">
    
    <div class="input-group date openingDate" data-date-format="yyyy-mm-ddThh:00:00Z" data-link-field="dtp_input1">
           <input class="form-control" name="openDate" size="16" type="text" value="" readonly />
           <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
		   <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
    </div>
				<!--<input  placeholder="Duration" name="increment" class="pass" type="password" required="">-->
				
                <select placeholder="Duration" name="increment" class="form-control">
					<option value="">Duration</option>
                    <option selected="2" value="2">2h.</option>
					<?php for($i=4;$i<=18;$i+=4):
						echo '<option value="'.$i.'">'.$i.'h.</option>';
					endfor; ?>
				</select>
                
                <!--Si el captcha falla que se le notifique al usuario-->
    		<?php if ($_GET["erroruser"]=="si"||$_GET["captchafilled"]=="no"):?>
    		<div style="text-align:center; color:gray; font-weight:bold">Bad Captcha...</div>
    		<?php endif;?>
            
      		<div class="g-recaptcha" data-sitekey="6LfBNg4UAAAAAKrh2SwA_lQtoO25E0I7m0TBsyBF"></div>
            	
                <?php if ($_GET["successful"]=="si"):?>
                <div style="text-align:center; margin-top:10px; color:gray; font-weight:bold">Upload successful!</div>
                <?php elseif($_GET["successful"]=="no"):?>
                <div style="text-align:center; margin-top:10px; color:gray; font-weight:bold">An error has ocurred</div>
                <?php endif;?>
                <input type="hidden" name="accion" value="enviarInfo" />
      			<input type="submit" id="contact-submit" data-submit="...Sending" value="Submit" class="btn btn-primary"/>
				<!--<input type="submit" value="sign up">-->
                
			</form>
		</div>
		<div class="clear"></div>
</div>

<!--footer-->
<div class="footer-w3">
	<p>&copy; 2017 Technidoo.</p>
    
    <!--<div class="footer-container"><iframe src="https://webchat.botframework.com/embed/DealPushBot?s=-MbHr6foMr8.cwA.oS4.uaHrs60rWMCcnCTWM4FAS74vW2blozm8JH3VVCIA9FU" style="height: 502px; max-height: 502px;"></iframe></div>-->
</div>
<!--//footer-->

<script type="text/javascript" src="./jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="./bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="./datetimepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="./datetimepicker/js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script type="text/javascript">
    $('.openingDate').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
		minView:1,
		minuteStep:60
        //showMeridian: 1
    });
	$('.form_time').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 1,
		minView: 0,
		maxView: 1,
		forceParse: 0
    });
</script>


</body>
</html>