<?php 

//Functions 
function aBd($palabra){
        $palabra = str_replace("'", "&apos;", $palabra);
        $palabra = str_replace('"', "&quot;", $palabra);
        return $palabra;
}

function esImagen($extension){
  $extension = strtolower($extension);
  if($extension=="jpg" || $extension=="bmp" || $extension=="png" || $extension=="gif" || $extension=="jpeg" || $extension=="tiff"): 
    return 1;
  else:
    return 0;
  endif;
}

//Variable declared as Global inside TDOEM
$http_code=1;

//Variables from USER form
$mytitle = aBd($_POST["title"]);
$mydescription = aBd($_POST["descript"]);
$myurl = aBd($_POST["url"]);
$zipcode=aBd($_POST["zipcode"]);
$date=$_POST["openDate"];
$increment=$_POST["increment"];

$myopeningDate = new DateTime($date);
$myendingDate = new DateTime($date);
$myendingDate->add(new DateInterval('PT'.$increment.'H'));

//Select the appropriate CampaignID
$campaign=$_POST["campaignID"];

//Auxiliary variables
$errorImg = 0;
$nImg1 = 'https://www.google.de/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwiA3fLz-e3WAhXDMBoKHR5BBIcQjRwIBw&url=http%3A%2F%2Fwww.picsymag.com%2Fpictures%2Fchristmas-pictures-for-xmas-2015%2F&psig=AOvVaw2aqu4kDh_V_ehZQTzN40E9&ust=1507996241253000';
$nImg2 = '';
$upload_addr='http://www.dailydealpush.de/dailydeal/';

	if (isset ($_FILES["userfile"])):
        $tot = count($_FILES["userfile"]["name"]); 
        for($i = 0; $i < $tot; $i++):
            $tmp_name = $_FILES["userfile"]["tmp_name"][$i];
            if(file_exists($tmp_name)):
                  $nombreAbs = $_FILES['userfile']["name"][$i];
                  $nnImg = explode(".", $nombreAbs);
                  $extension = end($nnImg);
                  $imagen = uniqid().'.'.$extension;
                  
                  $esImagen = esImagen($extension);
                  if($esImagen==1):
				    $myFile = "uploads/$imagen";
                    move_uploaded_file($_FILES["userfile"]['tmp_name'][$i], $myFile);
                    if($i==0):
					   //$nImg1=$_FILES["userfile"]['tmp_name'][$i];
                      $nImg2 = $upload_addr.''.$myFile;                            
                    elseif($i==1):
                       $nImg2=$upload_addr.''.$myFile;                            
                    endif;
                  else:
                    $errorImg = 1;
                 endif;
            endif;        
        endfor;
      endif;

//Class to make the POST request to TACO endpoint
class TdoemService {
	private $service = null;
	private $serviceUrl = 'https://taco.azurewebsites.net/api/TDOEM'; //Live Service
  	private $content_types = array('Content-type: text/plain'); 
  	private $username = 'TDOEMUser1';
  	private $password = '%!(6hm;äüö:23&2"'; 
	
	function __construct()
	{	
		// open curl Handle
		$this->service = curl_init();
		curl_setopt_array($this->service, array(
		CURLOPT_SSL_VERIFYPEER => FALSE,
		CURLOPT_HEADER => 0,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPHEADER => $this->content_types
		));
	}
	
	function __destruct()
	{
		// close curl Handle
		curl_close($this->service);
		unset ($this->service);
	}
	
	function tdoem() {
		$content_types = array('Content-type: text/plain'); 
		$service = curl_init();
		
		curl_setopt_array($service, array(
			CURLOPT_SSL_VERIFYPEER => FALSE,
			CURLOPT_HEADER => 1,
      		CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
      		CURLOPT_USERPWD => $this->username . ":" . $this->password,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $this->serviceUrl . '?action=campaign&campaignId=580df400117784713fdde24d',
			CURLOPT_HTTPHEADER => $content_types,
			CURLOPT_HTTPGET => true
		));
		
		$serviceData = curl_exec($service);
    
    	//echo    $serviceData;
		//echo base64_encode($username . ":" . $password);
    
    	// close curl Handle
		curl_close($service);
		//return $serviceData;
		return json_decode($serviceData);
	}
  
  function tdoemPost ($postData) {
		$content_types = array('Content-type: application/x-www-form-urlencoded');  
		$service = curl_init();
		
		curl_setopt_array($service, array(
			CURLOPT_SSL_VERIFYPEER => FALSE,
			CURLOPT_HEADER => 1,
      		CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
      		CURLOPT_USERPWD => $this->username . ":" . $this->password,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $this->serviceUrl . '',
			CURLOPT_HTTPHEADER => $content_types,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $postData
		));
		
		global $http_code;
		$serviceData = curl_exec($service);
		if(!curl_errno($service)){
			switch (curl_getinfo($service, CURLINFO_HTTP_CODE)){
				case 200:
					$http_code=200;
					break;
				default:
					$http_code=404;
			}
		}
    	//echo   $serviceData;
		// close curl Handle
		curl_close($service);
		//return $serviceData;
		return json_decode($serviceData,true);
	}
}

if($errorImg!=1):
    		$jsonEntry = '{"Title":"'.$mytitle.'","Description":"'.$mydescription.'","Url":"'.$myurl.'","ImageUrlFront":"'.$nImg1.'","ImageUrlBack":"'.$nImg2.'","OpeningDate":"'.$myopeningDate->format('c').'","EndDate":"'.$myendingDate->format('c').'","CampaignId":"'.$campaign.'"}';
			$fields = 'action=updateorinsertevent&payload='.base64_encode($jsonEntry);
			
$tdoemService = new TdoemService();
$tdoemService->tdoemPost($fields);  
	if($http_code==200):
		header("Location: index.php?successful=si"); 
	else:
		header("Location: index.php?successful=no");	
	endif;	      
else:
     header("Location: index.php?errorimage=si");
endif;	  
?>